var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var packageItems = require('./package-item-controller');
var packageItemsImages = require('./images/package-items-image-controller');

app.route('/api/package-items')
    .get(packageItems.list)
    .post(security.isAuthenticated, packageItems.create);

app.route('/api/package-items/:packageItemId')
    .get(packageItems.read)
    .put(security.isAuthenticated, packageItems.update)
    .delete(security.isAuthenticated, packageItems.delete);

app.route('/api/package-items/:packageItemId/image')
    .get(packageItemsImages.read)
    .post(packageItemsImages.upload);
    //.post(security.isAuthenticated, packageItemsImages.upload);

app.param('packageItemId', packageItems.packageItemByID);