var passport = require('passport');
var crypto = require('crypto');
var BasicStrategy = require('passport-http').BasicStrategy;

var adminPassword = 'Gl0b@l';

passport.use(new BasicStrategy(function (username, password, callback) {
    if (username != 'admin') return callback(null, false);
    var matches = hash(adminPassword) == password;
    callback(null, matches);
}));

function hash(text) {
    return crypto.createHash('sha256').update(text).digest('hex');
}

exports.isAuthenticated = passport.authenticate('basic', {session: false});
