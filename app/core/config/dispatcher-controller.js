var path = require('path');
var passport = require('passport');
var app = require(path.resolve('./app/app'));

app.use(loggingRequests);
app.use(passport.initialize());

require(path.resolve('./app/auto/engines/engine-routes'));
require(path.resolve('./app/auto/catalog_description/catalog_description-routes'));
require(path.resolve('./app/auto/package-items/package-item-routes'));
require(path.resolve('./app/auto/packages/package-routes'));
require(path.resolve('./app/auto/catalog/catalog-routes'));
require(path.resolve('./app/auto/vehicles/vehicle-routes'));
require(path.resolve('./app/users/user-routes'));
require(path.resolve('./app/core/security/authentication-routes'));

function loggingRequests(request, _, next) {
    console.log('Got \'' + request.method + '\' request on ' + request.url);
    next();
}
