var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var packages = require('./package-controller');
var packagesImages = require('./images/packages-image-controller');

app.route('/api/packages/search')
    .get(packages.search);

app.route('/api/package')
    .get( packages.list);

app.route('/api/package_limit')
    .get( packages.edit_list);

app.route('/api/packages_lenght')
    .get( packages.packages_lenght);

app.route('/api/packages_models')
    .get( packages.packages_models);

app.route('/api/packages')
    .get( packages.list)
    .post( packages.create);

app.route('/api/packages/:packageId')
    .get(packages.read)
    .put(security.isAuthenticated, packages.update)
    .delete(security.isAuthenticated, packages.delete);

app.route('/api/packages/:packageId/image')
    .get(packagesImages.read)
    .post(packagesImages.upload);
//.post(security.isAuthenticated, packageItemsImages.upload);

app.param('packageId', packages.packageByID);
