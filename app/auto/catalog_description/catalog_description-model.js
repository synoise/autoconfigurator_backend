var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CatalogDescriptionSchema = new Schema({
    model: {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle'
    },
    model_id:{ type: String },
    description: { type: String },
    img: { type: String },
    img_2: { type: String },
    img_3: { type: String },
    discount: { type: String },
    images: [{
        item_id: String,
    }],
});

CatalogDescriptionSchema.pre('save', function(next) {
    var now = new Date();
    this.updated = now;
    if (!this.created) this.created = now;
    next();
});

module.exports = mongoose.model('CatalogDescription', CatalogDescriptionSchema);
