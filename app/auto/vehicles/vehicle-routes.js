var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var vehicles = require('./vehicle-controller');
var vehicleImages = require('./images/vehicle-image-controller');

app.route('/api/vehicles')
    .get(vehicles.list)
    .post(security.isAuthenticated, vehicles.create);

app.route('/api/vehicles/:vehicleId')
    .get(vehicles.read)
    .put(security.isAuthenticated, vehicles.update)
    .delete(security.isAuthenticated, vehicles.delete);

app.route('/api/vehicles/:vehicleId/image')
    .get(vehicleImages.read)
    .post(vehicleImages.upload);
//.post(security.isAuthenticated, vehicleImages.upload);

app.param('vehicleId', vehicles.vehicleByID);

app.route('/api/vehicles_brand_list')
    .get(vehicles.brand_list);

app.route('/api/vehicles_body_type_list')
    .get(vehicles.body_type_list);

app.route('/api/vehicles_serial_name_list')
    .get(vehicles.serial_name_list);

app.route('/api/vehicles_items_list')
    .get(vehicles.brand_list);
