var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var catalog_description = require('./catalog_description-controller');

app.route('/api/catalog_description/search')
    .get(catalog_description.search);

app.route('/api/catalog_description/:engineId')
    .get(catalog_description.read)
    .put(security.isAuthenticated, catalog_description.update)
    .delete(security.isAuthenticated, catalog_description.delete);

app.route('/api/catalog_description')
    .get( catalog_description.list)
    .post(security.isAuthenticated, catalog_description.create);

app.param('engineId', catalog_description.engineByID);
