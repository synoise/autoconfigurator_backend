var http = require('http');
var path = require('path');
var app = require(path.resolve('./app/app'));
var port = require('./application-port');

var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function onListening() {
    console.log('Server started on port ' + port);
}

function onError(error) {
    switch (error.code) {
        case 'EADDRINUSE':
            console.log('Can\'t start app on port ' + port + ', it\'s already in use');
    }
}
