var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var Package = require('./package-model');

exports.create = function (req, res) {
    var pack = new Package(req.body);

    pack.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};

exports.search = function (req, res) {
    var query = {model: req.query.model};
    Package
        .find(query)
        .populate('model_extra_items.item')
        .populate('model')
        .populate('packages.items.item')
        .exec(function (err, engines) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(engines);
        });
};

exports.read = function (req, res) {
    var pack = req.pack ? req.pack.toJSON() : {};
    res.json(pack);
};

exports.update = function (req, res) {
    var pack = req.pack;

    pack.name = req.body.name;
    pack.model = req.body.model;
    pack.model_extra_items = req.body.model_extra_items;
    pack.packages = req.body.packages;

    pack.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};

exports.delete = function (req, res) {
    var pack = req.pack;

    pack.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};

exports.list = function (req, res) {
    Package
        .find()
        .sort('-created')
        .populate('model_extra_items.item')
        .populate('model')

        //.skip(Number(query_page * query_limit))
        // .limit(1)
        .exec(function (err, PackageItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(PackageItems);
        });
};

exports.packages_models = function (req, res) {
    // Package.aggregate(
    //     {
    //         $project: {
    //             item: {
    //                 model: '$model'
    //             }
    //         }
    //     },
    //
    //     function (err, docs) {
    //         var options = {
    //             path: 'item.model',
    //             model: 'Vehicle'
    //         };
    //
    //         Package.populate(docs, options, function (err, projects) {
    //             if (err) return res.status(422).send({
    //                 message: errorHandler.getErrorMessage(err)
    //             }); else {
    //                 res.json(projects)
    //             }
    //         });
    //
    //     });


    Package.aggregate(
        {
            $lookup: {
                from: 'vehicles',
                localField: "model",
                foreignField: "_id",
                as: "vehicles2"
            }
        },
        {
            $project: {
                model: '$model',
                model_id: '$vehicles2.model_id',
                body_type: '$vehicles2.body_type',
                brand: '$vehicles2.brand',
                name: '$vehicles2.name'
            }
        },

        // // {$limit: pageSize},
        // {$skip: Number(skipToItems * pageSize)},
        function (err, projects) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else {
                res.json(projects);
            }
        }
    );


};


exports.packages_lenght = function (req, res) {
    Package.aggregate(
        {
            $group: {_id: null, 'stock_sum': {$sum: 1}}
        },
        function (err, projects) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else {
                res.json(projects)
            }
        });
};

exports.edit_list = function (req, res) {

    var query_page = Number(req.query.page ? req.query.page : 0);
    var query_limit = Number(req.query.limit ? req.query.limit : 0);

    Package
        .find()
        .sort('-created')
        .populate('model_extra_items.item')
        .populate('model')
        .populate('packages.items.item')
        .skip(Number(query_page * query_limit))
        .limit(query_limit)
        .exec(function (err, PackageItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(PackageItems);
        });
};

exports.pack_list = function (req, res) {
    Package
        .find()
        .sort('-created')
        .populate('model_extra_items.item')
        .populate('model')
        .populate('packages.items.item')
        .exec(function (err, PackageItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(PackageItems);
        });
};

exports.packageByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'Package is invalid'
    });

    Package.findById(id)
        .populate('model_extra_items.item')
        .populate('model')
        .populate('packages.items.item')
        .exec(function (err, pack) {
            if (err)return next(err);
            if (!pack) return res.status(404).send({message: 'No pack with that identifier has been found'});
            req.pack = pack;
            next();
        })

};
