var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var catalog = require('./catalog-controller');
//var packagesImages = require('./images/packages-image-controller');

app.route('/api/catalog/search')
    .get(catalog.search);

app.route('/api/catalog/select')
    .get(catalog.select);

app.route('/api/catalog/selected_models')
    .get(catalog.selected_models);

app.route('/api/catalog/sendMail')
    .get(catalog.sendMail);

app.route('/api/catalog/select_length')
    .get(catalog.select);

app.route('/api/get_brand_list')
    .get(catalog.get_brand_list);

app.route('/api/get_body_type_list')
    .get(catalog.get_body_type_list);

app.route('/api/get_gear_list')
    .get(catalog.get_gear_list);

app.route('/api/get_type_list')
    .get(catalog.get_type_list);

app.route('/api/catalog_length')
    .get(catalog.length);

app.route('/api/catalog_page')
    .get(catalog.list_page);

app.route('/api/model_list')
    .get(catalog.model_list);

app.route('/api/catalog_get_page_size')
    .get(catalog.getPageSize);


app.route('/api/catalog')
    .get(catalog.list)
    .post(security.isAuthenticated, catalog.create);

app.route('/api/car/:catalogId')
    .get(catalog.read)


app.route('/api/catalog/:catalogId')
    .get(catalog.read)
    .put(catalog.update)
    .delete(catalog.delete);
//.post(security.isAuthenticated, packageItemsImages.upload);

app.param('catalogId', catalog.packageByID);
