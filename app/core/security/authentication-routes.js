var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require('./authentication-controller');

app.route('/api/login')
    .post(security.isAuthenticated, function (req, res) {
        res.json('Authenticated');
    });