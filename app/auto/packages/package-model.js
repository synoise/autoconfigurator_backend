var mongoose = require('mongoose');
var PackageItem = require('./../package-items/package-item-model');
var Vehicle = require('./../vehicles/vehicle-model');
var Schema = mongoose.Schema;

var PackageSchema = new Schema({
  model: {
    type: Schema.Types.ObjectId,
    ref: 'Vehicle'
  },
  model_extra_items: [{
    price: Number,
    item_id: String,
    images: Array,
    item: {
      type: Schema.Types.ObjectId,
      ref: 'PackageItem'
    }
  }],
  packages: [{
    package_id: String,
    name: String,
    items: [{
      price: Number,
      item_id: String,
      images: Array,
      item: {
        type: Schema.Types.ObjectId,
        ref: 'PackageItem'
      }
    }],
    //
    // inner_pack: [{
    //   price: Number,
    //   item_id: String,
    //   images: Array,
    //   item: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'PackageItem'
    //   }
    // }],
    color: Object,
    // upholster: [{ 
    //   price: Number,
    //   item_id: String,
    //   images: Array,
    //   item: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'PackageItem'
    //   }
    // }],
    price: Number,
    installment_price: Number,
    comment: String
  }],
  updated: Date,
  created: { type: Date, default: Date.now }
});

PackageSchema.pre('save', function(next) {
  var now = new Date();
  this.updated = now;
  if (!this.created) this.created = now;
  next();
});

module.exports = mongoose.model('Package', PackageSchema);
