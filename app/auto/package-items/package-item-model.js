var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PackageItemSchema = new Schema({
  item_id: { type: String },
  img: { type: String },
  name: { type: String },
  nazwa: { type: String },
  brand: { type: String },
  equ_group: { type: String },
  equ_sub_group: { type: String },
  comment: { type: String },
  komentarz: { type: String },

  // color parameters
  hex: { type: String },
  rgb: { type: String },

  // wheels parameters
  size: { type: Number },

  updated: { type: Date },
  created: { type: Date, default: Date.now }
});

PackageItemSchema.pre('save', function(next) {
  var now = new Date();
  this.updated = now;
  if (!this.created) this.created = now;
  next();
});

module.exports = mongoose.model('PackageItem', PackageItemSchema);
