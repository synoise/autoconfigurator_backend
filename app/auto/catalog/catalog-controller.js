var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var nodemailer = require('nodemailer');
//cashing = require('../../core/config/cache.js');

var mongoose = require('mongoose');
var Catalog = require('./catalog-model');
var pageSize = 9;
img_link = "test";

//var transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');
var transporter = nodemailer.createTransport('smtps://synoiseband:pass@smtp.gmail.com');

exports.sendMail = function (req, res) {
    //var mail = replaceAll(req.query.email_adress, "%20", " ")
    var mail = replaceAll(req.query, "%20", " ");
    //console.log(mail,666);

    var mailOptions = {
        from: 'synoiseband@gmail.com>',
        to: 'synoiseband@gmail.com',
        subject: 'Nowe zamowienie',
        text: 'Nowe zamowienie',
        html: req.quer // html body
    };

    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log('Message error: '+error);
        }
        console.log('Message sent: ' + info.response);
    });

};

exports.create = function (req, res) {
    var pack = new Catalog(req.body);

    pack.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};

function replaceAll(str, find, replace) {
    var $r = "";
    while ($r != str) {
        $r = str;
        str = String(str).replace(find, replace);
    }
    return str;
}

// exports.select_old = function (req, res) {
//     var catalog_lenght;
//     console.log(req.query.queries);
//     var queries = req.query.queries
//     var skipToItems = req.query.page ? req.query.page : 0;
//     queries = replaceAll(queries, "this_brand", "this.brand")
//     queries = replaceAll(queries, "this_gear", "this.gear")
//     queries = replaceAll(queries, "this_type", "this.type")
//     queries = replaceAll(queries, "this_body_type", "this.body_type")
//     queries = replaceAll(queries, "this_body_type", "this.body_type")
//     queries = replaceAll(queries, "this_model_code", "thismodel_code")
//     queries = replaceAll(queries, "|", "'")
//     queries = replaceAll(queries, "-or-", " || ")
//     queries = replaceAll(queries, "-and-", " && ")
//     console.log(queries);
//
//     Catalog
//         .find({$where: queries})
//         .exec(function (err, catalog) {
//             if (err) return res.status(404).send({
//                 message: 'No engines with that identifier has been found'
//             }); else
//                 catalog_lenght = catalog.length
//
//             Catalog
//                 .find({$where: queries})
//                 .populate('model')
//                 .populate('options_string.item')
//                 .populate('engine')
//                 .limit(pageSize)
//                 .skip(Number(skipToItems * pageSize))
//                 .exec(function (err, catalog) {
//                     if (err) return res.status(404).send({
//                         message: 'No engines with that identifier has been found'
//                     }); else res.json({catalog_lenght: catalog_lenght, catalog: catalog});
//                 });
//         });
// };


exports.getSelectLength = function (req, res) {
    Catalog
        .aggregate(
            {
                $match: {
                    model_code: req.query.model_id
                }
            },
            function (err, projects) {

                if (err) return res.status(422).send({
                    message: errorHandler.getErrorMessage(err)
                }); else {
                    res.json({_lenght: projects.length});
                }
            }
        )

};

var CatalogDescription = require('../catalog_description/catalog_description-model');

exports.select = function (req, res) {

    var query_page = Number(req.query.page ? req.query.page : 0)
    var query_limit = Number(req.query.limit ? req.query.limit : pageSize)

    var query = {model_code: req.query.model_id};

    Catalog
        .aggregate(
            {
                $match: {
                    model_code: req.query.model_id
                }
            },
            {
                $lookup: {
                    from: 'engines',
                    localField: "engine",
                    foreignField: "_id",
                    as: "engine1"
                }
            },
            {
                $lookup: {
                    from: 'vehicles',
                    localField: "model",
                    foreignField: "_id",
                    as: "vehicles2"
                }
            },

            // {
            //     $lookup: {
            //         from: 'packageitems',
            //         localField: "options_string.item_id",
            //         foreignField: "item_id",
            //         as: "options_string2"
            //     }
            // },

            {
                $project: {

                    brand: '$brand',
                    body_type: '$body_type',
                    serial_name: '$serial_name',
                    model_id: '$model_code',
                    img: img_link +  '$vehicles2.img',
                    seria: '$seria',
                    body_group: '$body_group',
                    model_code: '$model_code',
                    decription: '$decription',
                    color: '$engine1.color',
                    // upholster: '$engine1.upholster',
                    doors: '3',
                    type: '$engine1.type',
                    gear: '$engine1.gear',
                    drive: '$engine1.drive',
                    boost:'$engine1.boost',
                    capacity:'$engine1.capacity',
                    //model: '$model.serial_name',
                    // engine: '$engine',
                    options_string: '$options_string',
                    //options_string:{name:'$options_string2.name',item_id:'$options_string2.item_id',comment:'$options_string2.name'},
                    price: '$price',
                    price_engine: '$engine1.price',
                    power: '$engine1.power',
                    //engine1: '$engine1'
                }
            },
            {$skip: Number(query_page * query_limit)},
            {$limit: Number(query_limit)},
            function (err, docs) {
                var options =
                {
                    path: 'options_string.item',
                    model: 'PackageItem'
                };

                Catalog.populate(docs, options, function (err, projects) {
                    if (err) return res.status(422).send({
                        message: errorHandler.getErrorMessage(err)
                    }); else {
                        res.json(projects)
                    }
                });

            }
        )
// .populate('model')
// .populate('options_string.item')
// .populate('engine')
// .exec(function (err, catalog) {
//     if (err) return res.status(404).send({
//         message: 'No engines with that identifier has been found'
//     }); else res.json(catalog);
// });


}

exports.selected_models = function (req, res) {
    var query_page = Number(req.query.page ? req.query.page : 0)
    var query_limit = Number(req.query.limit ? req.query.limit : pageSize)
    var query = {model_code: req.query.model_id};
    Catalog
        .aggregate(
            {
                $match: {
                    model_code: req.query.model_id
                }
            },
            {
                $lookup: {
                    from: 'engines',
                    localField: "engine",
                    foreignField: "_id",
                    as: "engine1"
                }
            },
            {
                $lookup: {
                    from: 'vehicles',
                    localField: "model",
                    foreignField: "_id",
                    as: "vehicles2"
                }
            },
            {
                $project: {

                    brand: '$brand',
                    brand2: '$vehicles2.brand',
                    body_type: '$body_type',
                    serial_name: '$serial_name',
                    model_id: '$model_code',
                   // img: { $concat: [  img_link,  {$min: '$vehicles2.img'  }   ] },
                   img: '$vehicles2.img',

                    seria: '$seria',
                    body_group: '$body_group',
                    model_code: '$model_code',
                    decription: '$decription',
                    color: '$engine1.color',
                    // upholster: '$engine1.upholster',
                    doors: '3',
                    boost:'$engine1.boost',
                    capacity:'$engine1.capacity',
                    type: '$engine1.type',
                    gear: '$engine1.gear',
                    drive: '$engine1.drive',
                    //model: '$model.serial_name',
                    // engine: '$engine',
                    options_string: '$options_string',
                    //options_string:{name:'$options_string2.name',item_id:'$options_string2.item_id',comment:'$options_string2.name'},
                    price: '$price',
                    price_engine: '$engine1.price',
                    power: '$engine1.power',
                }
            },
            {$skip: Number(query_page * query_limit)},
            {$limit: Number(query_limit)},
            function (err, docs) {
                var options =
                {
                    path: 'options_string.item',
                    model: 'PackageItem'
                };

                Catalog.populate(docs, options, function (err, projects) {
                    if (err) return res.status(422).send({
                        message: errorHandler.getErrorMessage(err)
                    }); else {

                            var query = {model_id: req.query.model_id};

                            CatalogDescription
                                .aggregate(
                                    {
                                        $match: {
                                            model_id: req.query.model_id
                                        }
                                    },
                                    {
                                        $project: {

                                            description: '$description',
                                            img: img_link + '$img',
                                            serial_name: '$serial_name',
                                            model_id: '$model_code',
                                            img_2: img_link +  '$vehicles2.img',
                                            discount: '$discount'
                                        }
                                    },
                                    {$limit:1},
                        function (err, decs) {
                             res.json({'decs':decs,'projects':projects})

                        })

                       // res.json(docs)
                        // if (err) return res.status(422).send({
                        //     message: errorHandler.getErrorMessage(err)
                        // }); else {
                        //
                        //     res.json(projects)
                        // }


                       // res.json(projects)
                    }
                });

            }
        )

};



exports.search = function (req, res) {
    var query = {model: req.query.model};
    Catalog
        .find(query)
        .populate('model')
        .populate('options_string.item')
        .populate('engine')
        .exec(function (err, catalog) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(catalog);
        });
};


exports.get_body_type_list = function (req, res) {
    Catalog.distinct("body_type")
        .exec(function (err, catalogItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(catalogItems);
        });

}

exports.get_gear_list = function (req, res) {
    Catalog.distinct("gear")
        .exec(function (err, catalogItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(catalogItems);
        });
}

exports.get_type_list = function (req, res) {
    Catalog.distinct("type")
        .exec(function (err, catalogItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(catalogItems);
        });
}

exports.get_brand_list = function (req, res) {
    Catalog
        .distinct("brand")
        .exec(function (err, catalog) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(catalog);
        });
};


exports.read = function (req, res) {
    var pack = req.pack ? req.pack.toJSON() : {};
    console.log(pack)
    // pack
    // var options = [{
    //     path: 'model',
    //     model: 'Vehicle'
    // }, {
    //     path: 'engine',
    //     model: 'Engine'
    // }, {
    //     path: 'options_string.item',
    //     model: 'PackageItem'
    // }
    // ];
    //
    // Catalog.populate(pack, options, function (err, projects) {
    //     if (err) return res.status(422).send({
    //         message: errorHandler.getErrorMessage(err)
    //     }); else {
    //         res.json(projects)
    //     }
    // });


    Catalog
        .aggregate(
            {
                $match: {
                    _id: pack._id
                }
            },
            {
                $lookup: {
                    from: 'engines',
                    localField: "engine",
                    foreignField: "_id",
                    as: "engine1"
                }
            },
            {
                $lookup: {
                    from: 'vehicles',
                    localField: "model",
                    foreignField: "_id",
                    as: "vehicles2"
                }
            },

            // {
            //     $lookup: {
            //         from: 'packageitems',
            //         localField: "options_string.item_id",
            //         foreignField: "item_id",
            //         as: "options_string2"
            //     }
            // },

            {
                $project: {

                    brand: '$brand',
                    body_type: '$body_type',
                    serial_name: '$serial_name',
                    model_id: '$model_code',
                    img: '$vehicles2.img',
                    seria: '$seria',
                    body_group: '$body_group',
                    model_code: '$model_code',
                    decription: '$decription',
                    color: '$engine1.color',
                    // upholster: '$engine1.upholster',
                    doors: '3',
                    type: '$engine1.type',
                    gear: '$engine1.gear',
                    boost: '$engine1.boost',
                    drive: '$engine1.drive',
                    capacity: '2.0',
                    //model: '$model.serial_name',
                    // engine: '$engine',
                    options_string: '$options_string',
                    //options_string:{name:'$options_string2.name',item_id:'$options_string2.item_id',comment:'$options_string2.name'},
                    price: '$price',
                    price_engine: '$engine1.price',
                    power: '$engine1.power',
                    //engine1: '$engine1'
                }
            },
            function (err, docs) {
                var options =
                {
                    path: 'options_string.item',
                    model: 'PackageItem'
                };

                Catalog.populate(docs, options, function (err, projects) {
                    if (err) return res.status(422).send({
                        message: errorHandler.getErrorMessage(err)
                    }); else {
                        res.json(projects)
                    }
                });

            }
        )


    //res.json(pack);
};

exports.update = function (req, res) {
    var pack = req.pack;

    pack.production_no = req.body.production_no;
    pack.chassis_no = req.body.chassis_no;
    pack.body_group = req.body.body_group;
    pack.model_code = req.body.model_code;

    pack.brand = req.body.brand;
    pack.decription = req.body.decription;
    pack.color = req.body.color;
    pack.upholster = req.body.upholster;

    pack.dealer_no = req.body.dealer_no;
    pack.dealer_name = req.body.dealer_name;
    pack.location = req.body.location;
    pack.order_status = req.body.order_status;

    pack.order_date = req.body.order_date;
    pack.production_year = req.body.production_year;
    pack.week = req.body.week;
    pack.request = req.body.request;
    pack.options_string = req.body.options_string;
    pack.engine = req.body.engine;
    pack.vehicle = req.body.vehicle;

    pack.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};

exports.delete = function (req, res) {
    var pack = req.pack;
    pack.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(pack);
    });
};


exports.length = function (req, res) {
    Catalog.find()
        .exec(function (err, PackageItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            });
            else {
                res.json(exports.countPageSize(req.query.pagerCount, PackageItems.length));
            }
        });
};

exports.countPageSize = function (pagerCount, PackageItemsLength) {
    var allItem = PackageItemsLength;
    var toSend = allItem;

    if (typeof pagerCount != 'undefined') {
        toSend = allItem / pageSize;

        if (toSend > Math.round(toSend))
            toSend = Math.round(toSend) + 1;
        else
            toSend = Math.round(toSend);

        return toSend;
    }
};


exports.getPageSize = function (req, res) {
    res.json(pageSize);
};


exports.list = function (req, res) {
    Catalog
        .find()
        .sort('-created')
        .populate('model')
        .populate('options_string.item')
        .populate('engine')
        .exec(function (err, PackageItems) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(PackageItems);
        });
};


setName = function (name) {
    return name;
};

setSelecItemList = function (array, name) {

    var searchList = array.split(",");


    var searchList1 = []

    for (var i = 0; i < searchList.length; i++) {
        var obj = {};
        obj[name] = searchList[i];
        searchList1[i] = obj;
    }
    console.log(searchList1);

    return searchList1;
};

var Engine = require('../engines/engine-model');


var lookUp_1 = {
    $lookup: {
        from: 'engines',
        localField: "model_code",
        foreignField: "model_id",
        as: "engine1"
    }
};

var lookUp_2 = {
    $lookup: {
        from: 'vehicles',
        localField: "model_code",
        foreignField: "model_id",
        as: "vehicles2"
    }
};


var projector = {
    $project: {
        item: {
            brand: '$brand',
            brand2: '$vehicles2.brand',
            body_type: '$body_type',
            serial_name: '$vehicles2.serial_name',
            name: { $min: '$vehicles2.name' },
            model_id: '$model_code',
            type: '$type',
            gear: '$engine1.gear',
            img: '$vehicles2.img',
            drive: '$drive',
            price: '$price',
            price_engine: { $min: '$engine1.price' },
            power: '$engine1.power',
            doors: '3'
            //engine: '$engine1',
            //vehicles: '$vehicles2'
            // { $size: "$item" }
            // ,size :'44'
            // ,id333333333333333333 : { $size :'vehicles' }
        }
    }
};

var group = {
    $group: {_id: "$item"}
};

exports.list_page = function (req, res) {
    var skipToItems = req.query.page ? (req.query.page - 1 ) : 0;
    var searchObj = [{}];

    if (req.query.brand) {
        var brand = setSelecItemList(req.query.brand, 'brand');
        searchObj.push({$or: brand})
    }
    if (req.query.gear) {
        var gear = setSelecItemList(req.query.gear, 'gear');
        searchObj.push({$or: gear})
    }
    if (req.query.type) {
        var type = setSelecItemList(req.query.type, 'type');
        searchObj.push({$or: type});
    }
    if (req.query.body_type) {
        var body_type = setSelecItemList(req.query.body_type, 'body_type');
        searchObj.push({$or: body_type});
    }

    // if (req.query.name) {
    //     var name = setSelecItemList(replaceAll(req.query.name, "%20", " "), 'name');
    //     searchObj.push({$or: name});
    // }

    Catalog.aggregate(
                // lookUp_1,
                // lookUp_2,
                {
                    $match: {
                        $and: searchObj
                    }
                },
                projector,
        {
            $group: {_id:"$item"}
        },
        {
            $group: {_id:"item1",'total_sum': { $sum: 1 }}
        },

        // // {$limit: pageSize},
        // {$skip: Number(skipToItems * pageSize)},
        function (err, lenght1) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else {
                Catalog.aggregate(
                    lookUp_1,
                    lookUp_2,
                    {
                        $match: {
                            $and: searchObj
                        }
                    },
                    projector,
                    {
                        $group: {_id:"$item",'stock_sum': { $sum: 1 } }
                    },
                    {$skip: Number(skipToItems * pageSize)},
                    {$limit: pageSize},
                    function (err, projects) {
                        if (err) return res.status(422).send({
                            message: errorHandler.getErrorMessage(err)
                        }); else {
                            res.json({page_length: Math.ceil(lenght1[0].total_sum / pageSize), projects: projects})
                        }
                    });
            }
        }
        );
};

exports.model_list = function (req, res) {

    console.log(req.query.model_id);
    var modelId = req.query.model_id ? req.query.model_id : null;

    Catalog.aggregate(
        {$skip: 0},
        {
            $match: {
                model_code: modelId
            }
        },
        {
            $project: {
                item: {
                    brand: '$brand',
                    body_type: '$body_type',
                    serial_name: '$serial_name',
                    model_id: '$model_code',
                    type: '$type',
                    gear: '$gear',
                    img: '$model.img',
                    drive: '$drive'
                }
            }
        },

        function (err, docs) {
            var options = {
                path: 'model',
                model: 'Vehicle'
            };

            Catalog.populate(docs, options, function (err, projects) {
                if (err) return res.status(422).send({
                    message: errorHandler.getErrorMessage(err)
                }); else {
                    res.json(projects)
                }
            });

        });
};


exports.packageByID = function (req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'Catalog is invalid'
    });

    Catalog.findById(id)
        .populate('model')
        .populate('packages.item')
        .populate('engine')
        .exec(function (err, pack) {
            if (err)return next(err);
            if (!pack) return res.status(404).send({message: 'No pack with that identifier has been found'});
            req.pack = pack;
            next();
        });
};
