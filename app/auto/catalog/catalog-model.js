var mongoose = require('mongoose');
var PackageItem = require('./../package-items/package-item-model');
var Vehicle = require('./../vehicles/vehicle-model.js');
var Engines = require('./../engines/engine-model.js');
var Schema = mongoose.Schema;

var catalogSchema = new Schema({
  production_no: { type: Number },
  chassis_no: { type: String },
  seria: { type: String },
  body_group: { type: String },
  model_code: { type: String },
  brand: { type: String },
  decription: { type: String },
  color: { type: String },
  upholster: { type: String },
  dealer_no: { type: Number },
  dealer_name: { type: String },
  location: { type: String },
  order_status: { type: String },
  order_date: { type: String },
  production_year: { type: String },
  processing_type: { type: String },
  week: { type: String },
  request: { type: String },
  production_week: { type: String },
  production_date: { type: String },
  body_type: { type: String },
  type: { type: String },
  gear: { type: String },
  drive: { type: String },
  model: {
    type: Schema.Types.ObjectId,
    ref: 'Vehicle'
  },
  engine: {
    type: Schema.Types.ObjectId,
    ref: 'Engine'
  },
  engine_params:{type: Object},
  release_date: { type: String },
  options_string: [{
    item_id: String,
    item: {
      type: Schema.Types.ObjectId,
      ref: 'PackageItem'
    }
  }],
  price: Number,
  updated: Date,
  created: { type: Date, default: Date.now }
});

catalogSchema.pre('save', function(next) {
  var now = new Date();
  this.updated = now;
  if (!this.created) this.created = now;
  next();
});

module.exports = mongoose.model('Catalog', catalogSchema);