var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var Engine = require('./engine-model');
var Vehicle = require('../vehicles/vehicle-model');

exports.create = function (req, res) {
    var engine = new Engine(req.body);
    engine.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(engine);
    });
};

var pageSize = 20;


exports.search_id = function (req, res) {
    var query = {model: req.query.model};
    Engine
        .find(query)
        .populate('model')
        .exec(function (err, engines) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(engines);
        });
};

exports.search = function (req, res) {
    var query = {model_id: req.query.model_id};
    Engine
        .find(query)
        .populate('model')
        .exec(function (err, engines) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(engines);
        });
};

setSelecItemList = function (array, name) {
    var searchList = array.split(",");
    var searchList1 = []
    for (var i = 0; i < searchList.length; i++) {
        var obj = {};
        obj[name] = searchList[i];
        searchList1[i] = obj;
    }
    return searchList1;
};


exports.searchEngines = function (req, res) {


    var searchObj = [];

    if (req.query.serial_name) {
        var serial_name = setSelecItemList(req.query.serial_name, 'serial_name');
        searchObj.push({$or: serial_name})
    }
    ;


    console.log(serial_name, searchObj, req.query.serial_name);

    Engine.aggregate(
        {
            $lookup: {
                from: 'vehicles',
                localField: "model",
                foreignField: "_id",
                as: "vehicles1"
            }
        },
        {
            $project: {

                brand: '$vehicles1.brand',
                body_type: '$vehicles1.body_type',
                serial_name: '$vehicles1.serial_name',
                name_vehicle: '$vehicles1.name',
                img: '$vehicles1.img',
                model: '$model',
                doors: '$doors',
                // _id: '$_id',
                //engine1: '$engine1',

                model_id: '$model_id',
                gear: '$gear',
                capacity: '$capacity',
                drive: '$drive',
                power: '$power',
                fuel_consumption: '$fuel_consumption',
                price: '$price',
                boost: '$boost',
                co2: '$co2',
                price_part: '$price_part',
                name: '$name',
                type: '$type'
                // vehicles: '$vehicles1'

                // engine: {
                //     model_id: '$engine1.model_id',
                //     gear: '$engine1.gear',
                //     power: '$engine1.power',
                //     fuel_consumption: '$engine1.fuel_consumption',
                //     price: '$engine1.price',
                //     boost: '$engine1.boost',
                //     co2: '$engine1.co2',
                //     price_part: '$engine1.price_part',
                //     name: '$engine1.name',
                //     type: '$engine1.model_id'
                // }
            }
        },
        {
            $match: {
                $and: searchObj
            }
        },
        function (err, projects) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else {
                res.json(projects)
            }
        });
};

exports.read = function (req, res) {
    var engine = req.engine ? req.engine.toJSON() : {};
    res.json(engine);
};

exports.update = function (req, res) {


    if (!mongoose.Types.ObjectId.isValid(req.body._id)) return res.status(400).send({
        message: 'Engine is invalid'
    });

    Engine.findById(req.body._id).exec(function (err, engine1) {
        if (err)return next(err);

        if (!engine1) return res.status(404).send({
            message: 'No vehicle with that identifier has been found'
        });

        req.engine = engine1;
        var engine = req.engine;
        engine.model = req.body.model;
        engine.model_id = req.body.model_id;
        engine.name = req.body.name;
        engine.gear = req.body.gear;
        engine.power = req.body.power;
        engine.drive = req.body.drive;
        engine.type = req.body.type;
        engine.fuel_consumption = req.body.fuel_consumption;
        engine.price = req.body.price;
        engine.price_part = req.body.price_part;
        engine.boost = req.body.boost;
        engine.co2 = req.body.co2;
        engine.capacity = req.body.capacity;
        engine.doors = req.body.doors;
        engine._id = req.body._id;

        engine.save(function (err) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(engine);
        });

    });
};

exports.delete = function (req, res) {
    var Engine = req.engine;

    Engine.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(Engine);
    });
};

exports.list = function (req, res) {
    Engine
        .find()
        .limit(20)
        .sort('-created')
        .populate('model')
        .exec(function (err, Engines) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(Engines);
        });
};

exports.page_list = function (req, res) {
    var skipToItems = req.query.page ? req.query.page : 0;

    // if (req.query.model_id)
    var queries = req.query.model_id ? {model_id: req.query.model_id} : ''
    // else
    //     var queries = ""
    //
    // console.log(queries);
    // console.log(skipToItems);

    Engine
        .find(queries)
        .limit(pageSize)
        .skip(Number(skipToItems * pageSize))
        .sort('-created')
        .populate('model')
        .exec(function (err, Engines) {
            var lengthEng;
            Engine
                .find(queries)
                .exec(function (err, Engines1) {
                    lengthEng = Engines1.length;

                    if (err) return res.status(422).send({
                        message: errorHandler.getErrorMessage(err)
                    }); else res.json({engines_length: lengthEng, engines: Engines});
                });

        });
};

exports.engineByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'Engine is invalid'
    });

    Engine.findById(id).exec(function (err, engine) {
        if (err)return next(err);

        if (!engine) return res.status(404).send({
            message: 'No vehicle with that identifier has been found'
        });
        req.engine = engine;
        res.json(engine);
        // next();
    });
};
