var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var PackageItem = require('./package-item-model');

exports.create = function (req, res) {
    var packageItem = new PackageItem(req.body);
    packageItem.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(packageItem);
    });
};

exports.read = function (req, res) {
    var packageItem = req.packageItem ? req.packageItem.toJSON() : {};
    res.json(packageItem);
};

exports.update = function (req, res) {
    var packageItem = req.packageItem;

    packageItem.item_id = req.body.item_id;
    packageItem.img = req.body.img;
    packageItem.name = req.body.name;
    packageItem.brand = req.body.brand;
    packageItem.equ_group = req.body.equ_group;
    packageItem.equ_sub_group = req.body.equ_sub_group;
    packageItem.comment = req.body.comment;
    packageItem.nazwa = req.body.nazwa;
    packageItem.komentarz = req.body.komentarz;
    // color parameters
    packageItem.hex = req.body.hex;
    packageItem.rgb = req.body.rgb;
    // wheels parameters
    packageItem.size = req.body.size;

    packageItem.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(packageItem);
    });
};

exports.delete = function (req, res) {
    var packageItem = req.packageItem;
    packageItem.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(packageItem);
    });
};

exports.list = function (req, res) {
    PackageItem.find().sort('-created').exec(function (err, PackageItems) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(PackageItems);
    });
};

exports.packageItemByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'PackageItem is invalid'
    });

    PackageItem.findById(id).exec(function (err, packageItem) {
        if (err)return next(err);
        if (!packageItem) return res.status(404).send({
            message: 'No packageItem with that identifier has been found'
        });
        req.packageItem = packageItem;
        next();
    });
};
