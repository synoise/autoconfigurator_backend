var path = require('path');
var app = require(path.resolve('./app/app'));
var users = require('./user-controller');

app.route('/api/users')
    .get(users.list)
    .post(users.create);

app.route('/api/users/:engineId')
    .get(users.read)
    .put(users.update)
    .delete(users.delete);

app.param('userId', users.userByID);
