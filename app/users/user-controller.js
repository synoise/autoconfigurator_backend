var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var User = require('./user-model');

exports.create = function (req, res) {
    var user = new User(req.body);
    user.save(function (err) {
        if (err)
            return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            });
        else
            res.json(user);
    });
};

exports.read = function (req, res) {
    var user = req.user ? req.user.toJSON() : {};
    res.json(user);
};

exports.update = function (req, res) {
    var user = req.user;
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.username = req.body.username;
    user.password = req.body.password;

    user.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(user);
    });
};

exports.delete = function (req, res) {
    var user = req.user;
    user.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(user);
    });
};

exports.list = function (req, res) {
    User.find().sort('-created').exec(function (err, users) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(users);
    });
};

exports.userByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'user is invalid'
    });

    User.findById(id).exec(function (err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({
            message: 'No user with that identifier has been found'
        });
        req.user = user;
        next();
    });
};
