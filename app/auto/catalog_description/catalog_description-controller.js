var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var CatalogDescription = require('./catalog_description-model');

exports.create = function (req, res) {
    var engine = new CatalogDescription(req.body);
    engine.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(engine);
    });
};

exports.search = function (req, res) {
    var query = {model_id: req.query.model};
    CatalogDescription
        .find(query)
        .populate('model')
        .exec(function (err, engines) {
            if (err) return res.status(404).send({
                message: 'No engines with that identifier has been found'
            }); else res.json(engines);
        });
};

exports.read = function (req, res) {
    var engine = req.engine ? req.engine.toJSON() : {};
    res.json(engine);
};

exports.update = function (req, res) {
    var engine = req.engine;
    engine.model = req.body.model;
    engine.name = req.body.name;
    engine.gear = req.body.gear;
    engine.power = req.body.power;
    engine.drive = req.body.drive;
    engine.type = req.body.type;
    engine.fuel_consumption = req.body.fuel_consumption;
    engine.price = req.body.price;
    engine.price_part = req.body.price_part;
    engine.boost = req.body.boost;
    engine.co2 = req.body.co2;

    engine.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(engine);
    });
};

exports.delete = function (req, res) {
    var CatalogDescription = req.engine;

    CatalogDescription.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(CatalogDescription);
    });
};

exports.list = function (req, res) {
    CatalogDescription
        .find()
        .sort('-created')
        .exec(function (err, CatalogDescriptions) {
            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else res.json(CatalogDescriptions);
        });
};

exports.engineByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'CatalogDescription is invalid'
    });

    CatalogDescription
        .findById(id)
        .exec(function (err, engine) {
            if (err) return next(err);
            if (!engine) return res.status(404).send({
                message: 'No engine with that identifier has been found'
            });
            req.engine = engine;
            next();
        });
};
