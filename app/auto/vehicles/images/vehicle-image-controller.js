var multer = require('multer');
var service = require('./vehicle-image-service');

exports.upload = function (req, res) {
    service.upload(req, res, function (err) {
        if (err) return res.end(err + '');
        res.end("File is uploaded");
    })
};

exports.read = function (req, res) {
    service.find(req.vehicle, function (err, image) {
        if (err) return res.end(err + '');
        res.sendFile(image)
    });
};




