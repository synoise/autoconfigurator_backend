params = require('../../../local-config');
var multer = require('multer');
var glob = require('glob');
var fs = require('fs');

var repository = imagesPath+'/images/packages';
var acceptable_xt = '(.jpg|.jpeg|.png)';
var one_mb = 1000 * 1000;

function getExtensionFrom(filename) {
    var extStart = filename.lastIndexOf('.');
    return filename.substring(extStart);
}

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        var brandFolder = repository + '/' + req.pack._id;
        if (!fs.existsSync(brandFolder)) fs.mkdirSync(brandFolder);
        callback(null, brandFolder)
    },
    filename: function (req, file, callback) {
        var xt = getExtensionFrom(file.originalname);
        callback(null,file.originalname);///////////////  req.vehicle._id
    }
});
var fileFilter = function (_, file, callback) {
    var xt = getExtensionFrom(file.originalname);
    if (acceptable_xt.contains(xt)) callback(null, true);
    else callback(new Error('Wrong file extension'));
};

module.exports.upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: { fileSize: one_mb }
}).single('packagesImages');

module.exports.find = function (packages, callback) {
    console.error(packages)
    if (!packages._id) callback(new Error("Can't find image for broken packages data"));
    glob(repository + '/' + packages._id + '@' + acceptable_xt, function (err, files) {
        if (err || files.length == 0) callback(new Error("Couldn't find image"));
        else callback(null, files[0]);
    });
};

