params = require('../../local-config');
var path = require('path');
var app = require(path.resolve('./app/app'));

var front_end_app = crossDomainAdress;

app.use(function (req, res, next) {

    var host = req.get('host');
    front_end_app.forEach(function(val, key){
        if (host.indexOf(val) > -1){
            res.setHeader('Access-Control-Allow-Origin', 'http://' + val);
        }
    });

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});
