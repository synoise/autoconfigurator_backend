var express = require('express');
module.exports = express();

require('./core/config/language-extension');
require('./core/config/mongoose-connection');
require('./core/config/cors-access');
require('./core/config/body-parser');
require('./core/config/dispatcher-controller');
require('./core/config/server-start');
