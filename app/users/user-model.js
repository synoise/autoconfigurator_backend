var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = Schema({
    first_name: String,
    last_name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    created: { type: Date, default: Date.now },
    updated: Date
});

UserSchema.methods.adjustNames = function () {
    this.first_name = this.first_name.capitalize();
    this.last_name = this.last_name.capitalize();
};

UserSchema.pre('save', function (next) {
    var now = new Date();
    this.updated = now;
    if (!this.created) this.created = now;
    next();
});

module.exports = mongoose.model('User', UserSchema);
