var path = require('path');
var app = require(path.resolve('./app/app'));
var security = require(path.resolve('./app/core/security/authentication-controller'));
var engines = require('./engine-controller');

app.route('/api/engines/search')
    .get(engines.search);

app.route('/api/engines/by_id')
    .get(engines.search_id);

app.route('/api/engines_search')
    .get(engines.searchEngines);

app.route('/api/engines/:engineId')
    .get(engines.read)
    .put(security.isAuthenticated, engines.update)
    .delete(security.isAuthenticated, engines.delete);

app.route('/api/engine/:engineId2')
    .put(security.isAuthenticated,engines.update);

app.route('/api/engines_page')
    .get(engines.page_list)

app.route('/api/engines')
    .get(engines.list)
    //.get(security.isAuthenticated, engines.list)
    .post(security.isAuthenticated, engines.create);

app.param('engineId', engines.engineByID);
app.param('engineId2', engines.update);
//app.param('vehicleId', vehicles.vehicleByID);