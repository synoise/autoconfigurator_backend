var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EngineSchema = new Schema({
    model: {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle'
    },
    model_id: { type: String },
    name: { type: String },
    gear: { type: String },
    drive: { type: String },
    capacity: { type: String },
    power: { type: String },
    doors: { type: String },
    type: { type: String },
    fuel_consumption: { type: String },
    price: { type: Number },
    price_part: { type: Number },
    boost: { type: Number },
    co2: { type: String },
    updated: { type: Date },
    created: { type: Date, default: Date.now }
});

EngineSchema.pre('save', function(next) {
    var now = new Date();
    this.updated = now;
    if (!this.created) this.created = now;
    next();
});

module.exports = mongoose.model('Engine', EngineSchema);
