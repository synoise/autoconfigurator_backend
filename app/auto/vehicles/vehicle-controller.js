var path = require('path');
var errorHandler = require(path.resolve('./app/core/controllers/errors-controller'));
var mongoose = require('mongoose');
var Vehicle = require('./vehicle-model');

exports.create = function (req, res) {
    var vehicle = new Vehicle(req.body);

    vehicle.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(vehicle);
    });
};

exports.read = function (req, res) {
    // convert mongoose document to JSON
    var vehicle = req.vehicle ? req.vehicle.toJSON() : {};
    res.json(vehicle);
};

exports.update = function (req, res) {
    var vehicle = req.vehicle;

    vehicle.brand = req.body.brand;
    vehicle.img = req.body.img;
    vehicle.name = req.body.name;
    vehicle.model_id = req.body.model_id;
    vehicle.group_code = req.body.group_code;
    vehicle.serial_name = req.body.serial_name;
    vehicle.body_type = req.body.body_type;

    vehicle.save(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(vehicle);
    });
};

exports.delete = function (req, res) {
    var Vehicle = req.vehicle;

    Vehicle.remove(function (err) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(Vehicle);
    });
};

exports.brand_list = function (req, res) {
    Vehicle.distinct("brand").exec(function (err, Vehicles) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(Vehicles);
    });
};

setSelecItemList = function (array, name) {
    var searchList = array.split(",");
    var searchList1 = []
    for (var i = 0; i < searchList.length; i++) {
        var obj = {};
        obj[name] = searchList[i];
        searchList1[i] = obj;
    }
    return searchList1;
};


exports.body_type_list = function (req, res) {

    if (req.query.filter) {
        var filter = setSelecItemList(req.query.filter, 'brand');
       var obj = { $or: filter}
    }else var obj = null;

    Vehicle.find(  obj  ).distinct("body_type").exec(function (err, Vehicles) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(Vehicles);
    });
};

exports.serial_name_list = function (req, res) {



    var searchObj = [{}];

    if (req.query.brand) {
        var brand = setSelecItemList(req.query.brand, 'brand');
        searchObj.push({$or: brand})
    };

    if (req.query.body_type) {
        var body_type = setSelecItemList(req.query.body_type, 'body_type');
        searchObj.push({$or: body_type});
    };

    console.log(searchObj);



    // Vehicle.find(   {$and: searchObj}  )
    //     .distinct("serial_name")
    //     .exec(function (err, Vehicles) {
    //     if (err) return res.status(422).send({
    //         message: errorHandler.getErrorMessage(err)
    //     }); else res.json(Vehicles);
    // });

    Vehicle.aggregate(
       // {$skip: Number(skipToItems * pageSize)},
       // {$limit: pageSize},

        {
            $match: {
                $and: searchObj
            }
        },
        {
            $project: {
                item1: {
                    brand: '$brand',
                    body_type: '$body_type',
                    serial_name: '$serial_name',
                    img: '$img'
                }
            }
        },
        {
            $group: {_id: "$item1"}
        },
        function (err, projects) {

            // Catalog.populate(docs, options, function (err, projects) {

            if (err) return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
            }); else {
                res.json(projects)
            }
            // });

        });



};

exports.items_list = function (req, res) {

    Vehicle.distinct("brand").exec(function (err, brand) {
        Vehicle.distinct("body_type").exec(function (err, body_type) {
            Vehicle.distinct("serial_name").exec(function (err, serial_name) {
                res.json([ {serial_name:serial_name},{body_type:body_type},{brand:brand}]);
            });
        });
    });


};


exports.list = function (req, res) {
    Vehicle.find().sort('-created').exec(function (err, Vehicles) {
        if (err) return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
        }); else res.json(Vehicles);
    });
};

exports.vehicleByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).send({
        message: 'Vehicle is invalid'
    });

    Vehicle.findById(id).exec(function (err, vehicle) {
        if (err)return next(err);
        if (!vehicle) return res.status(404).send({
            message: 'No vehicle with that identifier has been found'
        });
        req.vehicle = vehicle;
        next();
    });
};
