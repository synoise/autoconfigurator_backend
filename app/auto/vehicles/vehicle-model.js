var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VehicleSchema = new Schema({
  brand: { type: String },
  img: { type: String },
  name: { type: String },
  model_id: { type: String },
  doors: { type: String },
  group_code: { type: String },
  serial_name: { type: String },
  body_type: { type: String },
  updated: { type: Date },
  created: { type: Date, default: Date.now }
});

VehicleSchema.pre('save', function(next) {
  var now = new Date();
  this.updated = now;
  if (!this.created) this.created = now;
  next();
});

module.exports = mongoose.model('Vehicle', VehicleSchema);
